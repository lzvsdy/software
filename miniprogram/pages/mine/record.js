import * as echarts from '../../ec-canvas/echarts';
/***************画柱状图*********************/


Page({
  data: {
    
    startdateday:[],
    records:[],//这里是统计的数据 根据如上的数组进行循环渲染 需要从日历界面抓取像上面这样的数组
    ecBar: {
      
      // 如果想要禁止触屏事件，以保证在图表区域内触摸移动仍能滚动页面，
      // 就将 disableTouch 设为 true
      // disableTouch: true,
      onInit: function (canvas, width, height, dpr) {
        const barChart = echarts.init(canvas, null, {
          width: width,
          height: height,
          devicePixelRatio: dpr // new
        });
        var startdate=[1,1,1,1,1];//最近五次月经开始的时间
        var period=[5, 6, 5, 5, 5];//最近五次经期长度
        var cycle= [30, 27, 34, 34, 39];//最近五次周期长度
        var idealperiod='28';//理想周期
        canvas.setChart(barChart);
        barChart.setOption(getBarOption(startdate,period,cycle,idealperiod));
        return barChart;
      },
    }
  },
  onLoad: function () {
    wx.cloud.callFunction({
      name: "getday"
     })
    .then(res=>{
      this.setData({
        records:res.result.data
      })
      console.log("请求成功",res);
    })
    .catch(err=>{
      console.log("请求失败",err);
    })
    wx.cloud.callFunction({
      name: "getstartdate"
    })
    .then(res=>{
      this.setData({
        startdateday:res.result.data
      })
      console.log("请求成功",res);
    })
    .catch(err=>{
      console.log("请求失败",err);
    })
    
    
  },
  
  onReady() {
  }
  
});
// function getDaysBetween(startDate, enDate) {
//   const sDate = Date.parse(startDate)
//   const eDate = Date.parse(enDate)
//   if (sDate > eDate) {
//     return 0
//   }
//   // 这个判断可以根据需求来确定是否需要加上
//   if (sDate === eDate) {
//     return 1
//   }
//   const days = (eDate - sDate) / (1 * 24 * 60 * 60 * 1000)
//   return days
// }
// var startdate=[];//最近五次月经开始的时间
// var period=[5, 6, 5, 5, 5];//最近五次经期长度
// var cycle= [30, 27, 34, 34, 39 ];//最近五次周期长度
// var idealperiod='28';//理想周期

function getBarOption(startdate,period,cycle,idealperiod) {
  return {
    tooltip: {
      trigger: 'axis',
      axisPointer: {            // 坐标轴指示器，坐标轴触发有效
        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
      }
    },

    legend: { 
      itemWidth: 23,
      itemHeight:10,
      itemGap: 25,
      data: ['经期', '周期', '理想周期']
    },
    grid: {
      left: 20,
      right: 20,
      bottom: 15,
      top: 40,
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        axisTick: { show: false },
        data:startdate,//最近5次月经开始的时间
        axisLine: {
          lineStyle: {
            color: '#999'
          }
        },
        axisLabel: {
          color: '#999',
          // rotate:30,
          formatter:'{value}'
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        axisLine: {
          lineStyle: {
            color: '#999'
          }
        },
        axisLabel: {
          color: '#999'
        }
      }
     
    ],
    
    series: [
      {
        name: '经期',
        type: 'bar',
        // stack: 'all',
        barCategoryGap: "55%",
       z:'2',
       itemStyle:{
        borderRadius:[3,3,0,0],
        
      },
        label: {
          show: true,
          fontSize: 9,
         
        },
        color:'#8b7ff5',
        data: period,//最近5次经期长度
        
      },
      {
        name: '周期',
        type: 'bar',
        barCategoryGap: "55%",
        // stack:'all',
        itemStyle:{
          borderRadius:[3,3,0,0],
        },
       z:'1',
        borderRadius:5,
        barGap:'-100%',
        label: {
          normal: {
            fontSize:10,
            show: true,
            position: 'inside'
          }
        },
        color:'#c1bcf3',
        data:cycle,//最近5次周期长度
            
      },
      {
        name: '理想周期',
        type: 'line',
        markLine: {
          symbol:"none",
           lineStyle: {
             color: '#8b7ff5',
             cap: "square"
           },
           data: [{ 
               name: '理想周期',
               yAxis: idealperiod,//从设置界面获取
           }],        
         }   
      }
    ]
  };
}
/*以上是绘制柱状图的代码*/





/**************功能函数，获取柱状图所需的数据 */
/*1：需要最近5次月经开始的时间 作为x轴坐标
  2：最近5次周期以及经期长度作为bar值
  3：所有记录的{开始日期 周期长度 经期长度}结构体数组
*/
// var N = 31;
// var lastMounth = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
//                    //上个月的标记，数组长度32，对应每天的日期
// var thisMounth = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
//                     //这个月的标记，数组长度32，对应每天的日期
// //Mounth 的含义是，-1表示没有这天，0表示没有来姨妈，1表示来姨妈

// function setDay(day) {
//     //参数 day : int，表示当前的日期
//     thisMounth[day] = 1;
// }
// //功能：根据 getDay 函数，当在日历页面，点击来姨妈，修改当天日期

// function getDay(str) {
//     //参数 str : 后端存储的是 string 类型
//     //后端 string 的格式是 xxxx-xx-x/xx，也就是说月份永远俩位数，日期可能一位数可能俩位数
//     var len = str.length;
//     var day;
//     if (len === 10) {
//         //日期十位数
//         if (str[len - 2] === '1') day = 10;
//         else if (str[len - 2] === '2') day = 20;
//         else day = 30;
//         day += str[len - 1] - '0';
//     } else {
//         //日期个位数
//         day = str[len - 1] - '0';
//     }
//     return day;
// }
// //功能：从 string 类型中获取当前日期，再存储

// function getMounth(str) {
//     //参数 str : 后端存储的是 string 类型
//     var len = str.length;
//     var mounth;
//     if (str[5] === '1') mounth = 10;
//     else mounth = 0;
//     mounth += str[5] - '0'; 
//     return mounth;
// }
// //功能：从 string 类型中获取当前月份

// function change(leapOrMean) {
//     //参数 leapOrMean : int / bool 类型
//     //没有考虑 2 月份的问题，所以月份只有 30 和 31
//     //leapOrMean : 值域为 {0, 1}。值为 1 时，表示闰月；值为0时，表示平月
//     for (var i = 0; i <= N; i ++ ) {
//         lastMounth[i] = thisMounth[i];
//         thisMounth[i] = 0;
//     }
//     if (leapOrMean) thisMounth[N] = 0;
//     else thisMounth[N] = -1;
// }

// function judge() {
//     var leapOrMean = 0;
//     if (lastMounth[N] === -1) leapOrMean = 0;
//     else leapOrMean = 1;
//     return leapOrMean;
//     //1是闰月，0是平月
// }
// //功能：判断上个月(lastMounth)是不是闰月

// function getCycle(day) {
//     //参数 day : int，表示当前的日期
//     var lastDay = -1;
//     for (var i = day - 1; i >= 1; i -- ) {
//         if (thisMounth[i] === 1 && thisMounth[i - 1] === 0) {
//             lastDay = i;
//             break;
//         }
//     }
//     var cycle = -1;
//     var lastMounthStart = -1;
//     if (lastDay === -1) {
//         if (judge()) lastMounthStart = 31;
//         else lastMounthStart = 30;
//         if (lastMounth[lastMounthStart] === 0) {
//             lastDay = 1;
//             cycle = day;
//         } else {
//             for (var i = lastMounthStart; i; i -- ) {
//                 if (lastMounth[i] === 1 && lastMounth[i - 1] === 0) {
//                     lastDay = i;
//                     break;
//                 }
//             }
//         }
//         cycle = day + 31 - lastDay;
//         if (lastMounthStart === 31) cycle ++;
//     } else {
//         cycle = day - lastDay + 1;
//     }
//     return cycle;
// }
// //功能：根据参数 day 找到上一次来月经的距离现在几天（可以在来月经第一天调用，就能获得周期）

// function predictDay(cycle, day, leapOrMean) {
//     //参数 cycle : 周期（“我的”页面的“我的设置”）
//     //参数 day : 当前日期
//     //参数 leapOrMean : 本月润or平
//     var nextDay = day + cycle;
//     if (leapOrMean) {
//         nextDay %= 31;
//         if (nextDay === 0) nextDay = 31;
//     } else {
//         nextDay %= 30;
//         if (nextDay === 0) nextDay = 30;
//     }
//     return nextDay;
// }
// //功能：根据参数 day、cycle、leapOrMean 预测下一次来月经的时间

// function getPeriod(day) {
//     //参数 day : 当前日期
//     var endDay = -1;
//     var startDay = -1;
//     var period = -1;
//     for (var i = day - 1; i; i -- ) {
//         if (thisMounth[i] === 1) {
//             endDay = i;
//             break;
//         }
//     }
//     if (endDay === -1) {
//         var lastMounthStart = 30;
//         if (judge()) lastMounthStart = 31;
//         for (var i = lastMounthStart; i; i -- )
//             if (lastMounth[i] === 1) {
//                 endDay = i;
//                 break;
//             }
//         for (var i = endDay - 1; i; i -- ) {
//             if (lastMounth[i] === 0) {
//                 startDay = i + 1;
//                 break;
//             }
//         }
//         period = endDay - startDay + 1;
//     } else {
//         for (var i = endDay - 1; i; i -- ) {
//             if (thisMounth[i] === 0) {
//                 startDay = i + 1;
//                 break;
//             }
//         }
//         if (startDay === -1) {
//             if (judge()) {
//                 if (lastMounth[N] === 0) {
//                     startDay = 1;
//                     period = endDay;
//                 } else {
//                     for (var i = N; i; i -- ) {
//                         if (lastMounth[i] === 0) {
//                             startDay = i + 1;
//                             period = endDay + 31 - startDay + 1;
//                             break;
//                         }
//                     }
//                 }
//             } else {
//                 if (lastMounth[N - 1] === 0) {
//                     startDay = 1;
//                     period = endDay;
//                 } else {
//                     for (var i = N - 1; i; i -- ) {
//                         if (lastMounth[i] === 0) {
//                             startDay = i + 1;
//                             period = endDay + 30 - startDay + 1;
//                             break;
//                         }
//                     }
//                 }
//             }
//         } else period = endDay - startDay + 1;
//     }
//     return period;
// }
// //功能：根据参数 day 找到上一次来月经的周期
// var sData = [
//   {
//       startData: "-1",
//       period: -1,
//       cycle: -1
//   },
  
//   {
//       startData: "-1",
//       period: -1,
//       cycle: -1
//   },
  
//   {
//       startData: "-1",
//       period: -1,
//       cycle: -1
//   },
  
//   {
//       startData: "-1",
//       period: -1,
//       cycle: -1
//   },
  
//   {
//       startData: "-1",
//       period: -1,
//       cycle: -1
//   }
//   ];
//   /*
//   五组数据
//   startData : string 类型，表示的是上次的开始时间
//   period : int 类型，表示的是经期长度
//   cycle : int 类型，表示的是周期长度
//   */
  
//   function getMounth(str) {
//       //参数 str : 后端存储的是 string 类型
//       var len = str.length;
//       var mounth;
//       if (str[5] === '1') mounth = 10;
//       else mounth = 0;
//       mounth += str[6] - '0'; 
//       return mounth;
//   }
//   //功能：从 string 类型中获取当前月份
  
//   function turnIntToString(num) {
//       //参数 num : int
//       var ans = num.toString();
//       return ans;
//   }
//   //将 int 类型转化成 string 类型
  
//   function insertData(newData) {
//       //参数 newData : sData 类型
//       for (var i = 0; i < 4; i ++ )
//           sData[i] = sData[i + 1];
//       sData[4] = newData;
//   }
//   //插入新的数据
  
//   function getData() {
//       return sData;
//   }
//   //获取五次记录，需求三
  
//   function makeItStandard(str) {
//       //参数 str : string 类型，不标准的日期
//       var len = str.length;
//       if (len === 10) return str;
//       else {
//           return str.split('', 8).join('') + '0' + str[len - 1];
//       }
//   }
//   //获得标准的日期
  
//   function setData(str, p, c) {
//       /*
//       参数 str : string 类型，不标准的日期
//       参数 p : 经期长度
//       参数 c : 周期
//       */
//       var newData = [
//           {
//               startData: "-1",
//               period: -1,
//               cycle: -1
//           }
//       ];
//       newData[0].startData = makeItStandard(str);
//       newData[0].period = p;
//       newData[0].cycle = c;
//       return newData;
//   }
//   //获得 sData 类型的数据，就是个转换器
  
//   function getBar() {
//       var bar = [
//           {
//               period: -1,
//               cycle: -1
//           }, 
//           {
//               period: -1,
//               cycle: -1
//           }, 
//           {
//               period: -1,
//               cycle: -1
//           },  
//           {
//               period: -1,
//               cycle: -1
//           }, 
//           {
//               period: -1,
//               cycle: -1
//           }, 
//       ];
//       for (var i = 0; i < 5; i ++ ) {
//           bar.period = sData[i].period;
//           bar.cycle = sData[i].cycle;
//       }
//       return bar;
//   }
//   //获取最近的五次经期长度和周期