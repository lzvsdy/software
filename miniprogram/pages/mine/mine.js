// pages/mine/mine.js

Page({
  data: {
     avatarUrl: "",
     inputText: ''
  }, 
 
onInputText(e) {
    const self = this
    const value = e.detail.value
    if (value) {
      // wx.setStorage({
            
      //   key: 'userText',
        
      //   data: value,
       
      //   })
        wx.setStorageSync('userText', value)
    } // 监听用户输入的信息，一旦有内容输入进去，就会使用wx.setStorageSync('userText', value)设置usertext这个key的值，使用wx.getStorageSync('userText')可以得到usertext这个key的值
},

  onLoad: function (options) {
    const that=this
    
    wx.getStorage({

      key: 'avatarUrl',
      
      success: function(res) {
      console.log(res)
     
     that.setData({ avatarUrl:res.data})
      
      },
      
      })
   let userText = wx.getStorageSync('userText')
    if (userText) {
        that.data.inputText = userText
        that.setData(that.data)
    }
     
    // let avatarUrl = wx.getStorageSync('avatarUrl')
    // this.setData({
    //   avatarUrl: avatarUrl
    // })
  },
  onChooseAvatar(e) {
    const { avatarUrl } = e.detail 
     this.setData({
      avatarUrl,
    })
    wx.setStorage({
            
      key: 'avatarUrl',
      
      data: avatarUrl,
     
      })

    //  wx.setStorageSync('avatarurl', avatarUrl)
   
  },
  
  /**
   * 页面的初始数据
   */


  /**
   * 生命周期函数--监听页面加载
   */
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
gotorecord(){
 wx.navigateTo({
   url: '/pages/mine/record',
 })
},
gotoset(){
  wx.navigateTo({
    url: '/pages/mine/set',
  })
 },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})


